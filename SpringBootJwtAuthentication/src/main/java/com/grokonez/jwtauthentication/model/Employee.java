package com.grokonez.jwtauthentication.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="employees")
@Getter
@Setter
@AllArgsConstructor
public class Employee implements Serializable {

    private static  final long serialVersionUID = -43455434546L;

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long employeeId;

    @Column(unique = true, nullable = false, length = 50)
    private String lastName;

    @Column(unique = true, nullable = false, length = 50)
    private String firstName;

    @Column(unique = true, nullable = false, length = 50)
    private String extension;

    @Column(unique = true, nullable = false, length = 50)
    private String email;

    @Column(unique = true, nullable = false, length = 50)
    private String officeCode;

    @Column(unique = true, nullable = false, length = 50)
    private String reportsTo;

    @Column(unique = true, nullable = false, length = 50)
    private String jobTitle;

    public Employee(){

    }

}
