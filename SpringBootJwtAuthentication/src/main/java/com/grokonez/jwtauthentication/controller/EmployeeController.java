package com.grokonez.jwtauthentication.controller;


import com.grokonez.jwtauthentication.exception.ResourceNotFoundException;
import com.grokonez.jwtauthentication.model.Employee;
import com.grokonez.jwtauthentication.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    //
    @GetMapping("/employees")
    @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
//    @RequestMapping(value="/employees", method=RequestMethod.GET)
    public List<Employee> getAllEmployee(){
        return employeeRepository.findAll();
    }
   //Get By id
   // Get a Single Note
   @GetMapping("/employee/{id}")
   public Employee getEmployeeById(@PathVariable(value = "id") Long empId) {
       return employeeRepository.findById(empId)
               .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", empId));
   }

    @PostMapping("/employee")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
//    @RequestMapping(value="/employee", method=RequestMethod.POST)
    public Employee createEmployee(@Valid @RequestBody Employee emplFrom){
        return employeeRepository.save(emplFrom);
    }

    @PutMapping("/employee/{id}")
//    @RequestMapping(value="/employee", method=RequestMethod.PUT)
    public Employee updateEmployee(@PathVariable(value = "id") Long empId, @Valid @RequestBody Employee employeeDetail){
        Employee emp = employeeRepository.findById(empId).orElseThrow(() -> new ResourceNotFoundException("Employee","id","TEST"));
        emp.setLastName(employeeDetail.getLastName());
        emp.setEmail(employeeDetail.getEmail());
        emp.setFirstName(employeeDetail.getFirstName());
        emp.setExtension(employeeDetail.getExtension());
        emp.setJobTitle(employeeDetail.getJobTitle());
        emp.setOfficeCode(employeeDetail.getOfficeCode());
        emp.setReportsTo(employeeDetail.getReportsTo());
        Employee updateEmployee =   employeeRepository.save(emp);
        return updateEmployee;

    }

    @DeleteMapping("/employee/{id}")
    //@RequestMapping(value="/employee", method=RequestMethod.DELETE)
    public ResponseEntity<Employee> deleteEmployee(@PathVariable(value = "id") Long empId){
        Employee emp = employeeRepository.findById(empId).orElseThrow(() -> new ResourceNotFoundException("Employee","id",empId));
         employeeRepository.delete(emp);
        return ResponseEntity.ok().build();
    }
//    @Autowired
//    private EmployeeService employeeService;
//
//    //List all of employee
//    @RequestMapping(value = "/employees",
//            method = RequestMethod.GET,
//            produces = {
//                    MediaType.APPLICATION_JSON_VALUE,
//                    MediaType.APPLICATION_XML_VALUE
//            } )
//    @ResponseBody
//    public List<Employee> getEmployees(){
//        List<Employee> list = employeeService.getAllEmployees();
//        return list;
//    }
//
//
//    // list id of employee
//    @RequestMapping(value = "/employee/{empId}",
//            method = RequestMethod.GET,
//            produces = {
//                    MediaType.APPLICATION_JSON_VALUE,
//                    MediaType.APPLICATION_XML_VALUE
//            } )
//    @ResponseBody
//    public Employee getEmployee(@PathVariable("empId") Long empId){
//      return employeeService.getEmployee(empId);
//    }
//
//    // add of employee
//    @RequestMapping(value = "/employee",
//            method = RequestMethod.POST,
//            produces = {
//                    MediaType.APPLICATION_JSON_VALUE,
//                    MediaType.APPLICATION_XML_VALUE
//            } )
//    @ResponseBody
//    public Employee addEmployee(@RequestBody Employee empForm){
//        return employeeService.addEmployee(empForm);
//    }
//    // update of employee
//    @RequestMapping(value = "/employee/{empId}",
//            method = RequestMethod.PUT,
//            produces = {
//                    MediaType.APPLICATION_JSON_VALUE,
//                    MediaType.APPLICATION_XML_VALUE
//            } )
//    @ResponseBody
//    public Employee updateEmployee(@RequestBody Employee empForm){
//        return employeeService.updateEmployee(empForm);
//    }
//    // delete of employee
//    @RequestMapping(value = "/employee/{empId}",
//            method = RequestMethod.DELETE,
//            produces = {
//                    MediaType.APPLICATION_JSON_VALUE,
//                    MediaType.APPLICATION_XML_VALUE
//            } )
//    @ResponseBody
//    public String deleteEmployee(@PathVariable("empId") Long empId){
//         employeeService.deleteEmployee(empId);
//        return "delete success!!!";
//    }


}
